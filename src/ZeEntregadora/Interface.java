package ZeEntregadora;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.EOFException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.CardLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JTextArea;
import javax.swing.JScrollBar;

public class Interface {

	private JFrame frame;
	private JTextField textFieldCadPlaca;
	private JTextField textFieldExcluiPlaca;
	private Frota frota = new Frota();
	private Entrega pegamargem = new Entrega(0.0f,0.0f,0.0f);
	private JTextField textFieldCarga;
	private JTextField textFieldTempo;
	private JTextField textFieldDistancia;
	private JTextField textFieldFinalPlaca;
	private JTextField textFieldML;
	private String envianome1, envianome2, envianome3;
	private float enviatempo1, enviatempo2, enviatempo3;
	private float enviapreco1, enviapreco2, enviapreco3;

	/**
	 * Launch the application.
	 */
	public void maininterface(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface window = new Interface();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		frota.CriaArquivos();
		pegamargem.CriaArquivosDados();
	}

	/**
	 * Create the application.
	 */
	public Interface() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.DARK_GRAY);
		frame.setBackground(Color.DARK_GRAY);
		frame.setBounds(100, 100, 600, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel controlepanel = new JPanel();
		controlepanel.setBackground(Color.GRAY);
		
		JPanel basepanel = new JPanel();
		basepanel.setBackground(Color.GRAY);
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(controlepanel, GroupLayout.PREFERRED_SIZE, 163, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(basepanel, GroupLayout.DEFAULT_SIZE, 395, Short.MAX_VALUE)
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(basepanel, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 446, Short.MAX_VALUE)
						.addComponent(controlepanel, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 446, Short.MAX_VALUE))
					.addContainerGap())
		);
		basepanel.setLayout(new CardLayout(0, 0));
		
		JPanel veicpanel = new JPanel();
		veicpanel.setBackground(Color.GRAY);
		basepanel.add(veicpanel, "name_31528891266296");
		veicpanel.setLayout(null);
		
		JLabel lblCadastro = new JLabel("Cadastro:");
		lblCadastro.setForeground(Color.BLACK);
		lblCadastro.setBounds(12, 0, 114, 30);
		lblCadastro.setFont(new Font("Ubuntu", Font.BOLD, 20));
		veicpanel.add(lblCadastro);
		
		JLabel lblTipo = new JLabel("Tipo:");
		lblTipo.setForeground(Color.BLACK);
		lblTipo.setBounds(92, 37, 66, 15);
		veicpanel.add(lblTipo);
		
		JComboBox comboBoxTipo = new JComboBox();

		comboBoxTipo.setModel(new DefaultComboBoxModel(new String[] {"Carreta", "Van", "Carro", "Moto"}));
		comboBoxTipo.setBounds(102, 53, 149, 24);
		veicpanel.add(comboBoxTipo);
		
		JLabel lblCombustvel = new JLabel("Combustível:");
		lblCombustvel.setForeground(Color.BLACK);
		lblCombustvel.setBounds(92, 89, 108, 24);
		veicpanel.add(lblCombustvel);
		
		JComboBox comboBoxCombu = new JComboBox();
		comboBoxCombu.setModel(new DefaultComboBoxModel(new String[] {"Gasolina ", "Álcool"}));
		comboBoxCombu.setBounds(102, 114, 149, 24);
		veicpanel.add(comboBoxCombu);
		
		comboBoxTipo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(comboBoxTipo.getSelectedItem()== "Carro"||comboBoxTipo.getSelectedItem()=="Moto") {
					comboBoxCombu.setEnabled(true);
				}
				else {
					comboBoxCombu.setEnabled(false);
				}
			}
		});
		
		JLabel lblPlaca = new JLabel("Placa:");
		lblPlaca.setForeground(Color.BLACK);
		lblPlaca.setBounds(92, 150, 66, 15);
		veicpanel.add(lblPlaca);
		
		textFieldCadPlaca = new JTextField();
		textFieldCadPlaca.setBounds(102, 168, 149, 24);
		veicpanel.add(textFieldCadPlaca);
		textFieldCadPlaca.setColumns(10);
		
		JLabel lblExcluso = new JLabel("Exclusão:");
		lblExcluso.setForeground(Color.BLACK);
		lblExcluso.setFont(new Font("Ubuntu", Font.BOLD, 20));
		lblExcluso.setBounds(12, 267, 149, 24);
		veicpanel.add(lblExcluso);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String tipo, placa, combustivel;
				tipo = (String) comboBoxTipo.getSelectedItem();
				combustivel = (String) comboBoxCombu.getSelectedItem();
				placa = textFieldCadPlaca.getText();
				
				frota.CriaVeiculo(tipo, placa, combustivel);
				
				JOptionPane.showMessageDialog(veicpanel, "O veículo foi cadastrado!");
				
				//Removendo painéis:
				basepanel.removeAll();
				basepanel.repaint();
				basepanel.revalidate();
				//Adicionando painéis:
				basepanel.add(veicpanel);
				basepanel.repaint();
				basepanel.revalidate();
			}
		});
		btnCadastrar.setBounds(125, 219, 114, 25);
		veicpanel.add(btnCadastrar);
		
		JLabel lblExcluiPlaca = new JLabel("Placa:");
		lblExcluiPlaca.setForeground(Color.BLACK);
		lblExcluiPlaca.setBounds(92, 314, 66, 15);
		veicpanel.add(lblExcluiPlaca);
		
		textFieldExcluiPlaca = new JTextField();
		textFieldExcluiPlaca.setColumns(10);
		textFieldExcluiPlaca.setBounds(102, 339, 149, 24);
		veicpanel.add(textFieldExcluiPlaca);
		
		JButton btnExcluir = new JButton("Excluir");
		btnExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boolean result = frota.ApagaVeiculo(textFieldExcluiPlaca.getText());
				
				if(result==true) {
					// Veículo foi apagado
					JOptionPane.showMessageDialog(veicpanel, "O veículo foi apagado!");
					
					//Removendo painéis:
					basepanel.removeAll();
					basepanel.repaint();
					basepanel.revalidate();
					//Adicionando painéis:
					basepanel.add(veicpanel);
					basepanel.repaint();
					basepanel.revalidate();
				}
				else {
					//Esse veículo não existe ou está em serviço
					JOptionPane.showMessageDialog(veicpanel, "Esse veículo não existe ou está em serviço!");
				}
				
				
			}
		});
		btnExcluir.setBounds(125, 385, 114, 25);
		veicpanel.add(btnExcluir);
		
		JPanel entregapanel = new JPanel();
		entregapanel.setBackground(Color.GRAY);
		basepanel.add(entregapanel, "name_40943092767594");
		entregapanel.setLayout(null);
		
		JLabel lblRealizarEntrega = new JLabel("Realizar entrega:");
		lblRealizarEntrega.setForeground(Color.BLACK);
		lblRealizarEntrega.setFont(new Font("Ubuntu", Font.BOLD, 20));
		lblRealizarEntrega.setBounds(12, 12, 237, 29);
		entregapanel.add(lblRealizarEntrega);
		
		JLabel lblCarga = new JLabel("Carga:");
		lblCarga.setForeground(Color.BLACK);
		lblCarga.setFont(new Font("Dialog", Font.BOLD, 12));
		lblCarga.setBounds(38, 48, 66, 15);
		entregapanel.add(lblCarga);
		
		textFieldCarga = new JTextField();
		textFieldCarga.setBounds(117, 46, 124, 19);
		entregapanel.add(textFieldCarga);
		textFieldCarga.setColumns(10);
		
		JLabel lblkg = new JLabel("(KG)");
		lblkg.setForeground(Color.BLACK);
		lblkg.setBounds(259, 48, 66, 15);
		entregapanel.add(lblkg);
		
		JLabel lblTempo = new JLabel("Tempo:");
		lblTempo.setForeground(Color.BLACK);
		lblTempo.setBounds(38, 85, 66, 15);
		entregapanel.add(lblTempo);
		
		JLabel lblDistncia = new JLabel("Distância:");
		lblDistncia.setForeground(Color.BLACK);
		lblDistncia.setBounds(38, 123, 82, 15);
		entregapanel.add(lblDistncia);
		
		textFieldTempo = new JTextField();
		textFieldTempo.setColumns(10);
		textFieldTempo.setBounds(117, 83, 124, 19);
		entregapanel.add(textFieldTempo);
		
		textFieldDistancia = new JTextField();
		textFieldDistancia.setColumns(10);
		textFieldDistancia.setBounds(117, 121, 124, 19);
		entregapanel.add(textFieldDistancia);
		
		JLabel lblhoras = new JLabel("(Horas)");
		lblhoras.setForeground(Color.BLACK);
		lblhoras.setBounds(259, 85, 66, 15);
		entregapanel.add(lblhoras);
		
		JLabel lblkm = new JLabel("(Km)");
		lblkm.setForeground(Color.BLACK);
		lblkm.setBounds(259, 123, 66, 15);
		entregapanel.add(lblkm);
		
		JPanel novapanel = new JPanel();
		novapanel.setBackground(Color.GRAY);
		basepanel.add(novapanel, "name_55895426212333");
		novapanel.setLayout(null);
		
		
		JLabel lblCarreta = new JLabel("Carreta");
		lblCarreta.setBounds(141, 303, 66, 15);
		novapanel.add(lblCarreta);
		
		JLabel label = new JLabel("00:00:00");
		label.setBounds(207, 303, 66, 15);
		novapanel.add(label);
		
		JLabel lblR = new JLabel("R$ 100.000.000");
		lblR.setBounds(279, 303, 130, 15);
		novapanel.add(lblR);
		
		JLabel label_1 = new JLabel("Carreta");
		label_1.setBounds(141, 211, 66, 15);
		novapanel.add(label_1);
		
		JLabel label_2 = new JLabel("Carreta");
		label_2.setBounds(138, 121, 66, 15);
		novapanel.add(label_2);
		
		JLabel label_3 = new JLabel("00:00:00");
		label_3.setBounds(207, 211, 66, 15);
		novapanel.add(label_3);
		
		JLabel label_4 = new JLabel("00:00:00");
		label_4.setBounds(207, 121, 66, 15);
		novapanel.add(label_4);
		
		JLabel label_5 = new JLabel("R$ 100.000.000");
		label_5.setBounds(279, 211, 130, 15);
		novapanel.add(label_5);
		
		JLabel label_6 = new JLabel("R$ 100.000.000");
		label_6.setBounds(279, 121, 130, 15);
		novapanel.add(label_6);
		
		
		
		
		JButton btnConsultar = new JButton("Consultar");
		btnConsultar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Realizando os cálculos:
				float carga, distancia, tempo;
				boolean result;
				carga = Float.parseFloat(textFieldCarga.getText());
				distancia = Float.parseFloat(textFieldDistancia.getText());
				tempo = Float.parseFloat(textFieldTempo.getText());
				
				Entrega entrega = new Entrega(carga, distancia, tempo);
				
				result = entrega.Verifica_entrega();
				
				if(result==true) {
					// É possível realizar a entrega
				
				DecimalFormat df = new DecimalFormat("0.00");
				DecimalFormat onedf = new DecimalFormat("0.0");
				
				//Opção 1:
				envianome1 = entrega.getMenor_custo();
				label_2.setText(entrega.getMenor_custo());
				float auxtempo1 = entrega.getMenor_custo_tempo();
				enviatempo1 = auxtempo1;
				label_4.setText(onedf.format(auxtempo1)+ "h");
				float aux_preco_1 = entrega.getMenor_custo_preco();
				enviapreco1 = aux_preco_1;
				label_6.setText("R$" + df.format(aux_preco_1));
				
				//Opção 2:
				envianome2 = entrega.getMais_rapido();
				label_1.setText(entrega.getMais_rapido());
				float auxtempo2 = entrega.getMais_rapido_tempo();
				enviatempo2 = auxtempo2;
				label_3.setText(onedf.format(auxtempo2)+"h");
				float aux_preco_2 = entrega.getMais_rapido_preco();
				enviapreco2 = aux_preco_2;
				label_5.setText("R$" + df.format(aux_preco_2));
				
				//Opção 3:
				envianome3 = entrega.getMais_benefico();
				lblCarreta.setText(entrega.getMais_benefico());
				float auxtempo3 = entrega.getMais_benefico_tempo();
				enviatempo3 = auxtempo3;
				label.setText(onedf.format(auxtempo3)+"h");
				float aux_preco_3 = entrega.getMais_benefico_preco();
				enviapreco3 = aux_preco_3;
				lblR.setText("R$"+df.format(aux_preco_3));
				
				//Removendo painéis:
				basepanel.removeAll();
				basepanel.repaint();
				basepanel.revalidate();
				//Adicionando painéis:
				basepanel.add(novapanel);
				basepanel.repaint();
				basepanel.revalidate();
				}
				else {
					//Não é possível realizar a entrega
					JOptionPane.showMessageDialog(entregapanel, "Não há veículos capazes de realizar essa entrega!");
					//Removendo painéis:
					basepanel.removeAll();
					basepanel.repaint();
					basepanel.revalidate();
					//Adicionando painéis:
					basepanel.add(entregapanel);
					basepanel.repaint();
					basepanel.revalidate();
					
				}
			}
		});
		btnConsultar.setForeground(Color.BLACK);
		btnConsultar.setBounds(249, 166, 114, 25);
		entregapanel.add(btnConsultar);
		
		JLabel lblFinalizarEntrega = new JLabel("Finalizar entrega:");
		lblFinalizarEntrega.setForeground(Color.BLACK);
		lblFinalizarEntrega.setFont(new Font("Ubuntu", Font.BOLD, 20));
		lblFinalizarEntrega.setBounds(12, 240, 211, 29);
		entregapanel.add(lblFinalizarEntrega);
		
		JLabel lblPlacaDoVeculo = new JLabel("Placa do veículo enviado:");
		lblPlacaDoVeculo.setForeground(Color.BLACK);
		lblPlacaDoVeculo.setBounds(25, 314, 211, 15);
		entregapanel.add(lblPlacaDoVeculo);
		
		textFieldFinalPlaca = new JTextField();
		textFieldFinalPlaca.setBounds(231, 312, 124, 19);
		entregapanel.add(textFieldFinalPlaca);
		textFieldFinalPlaca.setColumns(10);
		
		JButton btnFinalizar = new JButton("Finalizar");
		btnFinalizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boolean result = frota.RetornaVeiculo(textFieldFinalPlaca.getText());
				if (result ==true) {
					//Foi possível remover o veiculo
					JOptionPane.showMessageDialog(entregapanel, "O veículo retornou com sucesso!");
				}
				else {
					//Não foi possível remover o veículo
					JOptionPane.showMessageDialog(entregapanel, "Esse veículo não está em serviço no momento!");
				}
			}
		});
		btnFinalizar.setForeground(Color.BLACK);
		btnFinalizar.setBounds(249, 372, 114, 25);
		entregapanel.add(btnFinalizar);
		
		JLabel lblNovaEntrega = new JLabel("Nova Entrega:");
		lblNovaEntrega.setForeground(Color.BLACK);
		lblNovaEntrega.setFont(new Font("Ubuntu", Font.BOLD, 20));
		lblNovaEntrega.setBounds(113, 12, 207, 37);
		novapanel.add(lblNovaEntrega);
		
		JLabel lblMenorCusto = new JLabel("Menor custo:");
		lblMenorCusto.setForeground(Color.BLACK);
		lblMenorCusto.setBounds(12, 118, 104, 20);
		novapanel.add(lblMenorCusto);
		
		JLabel lblVeculo = new JLabel("Veículo:");
		lblVeculo.setForeground(Color.BLACK);
		lblVeculo.setBounds(125, 75, 66, 15);
		novapanel.add(lblVeculo);
		
		JLabel lblTempo_1 = new JLabel("Tempo:");
		lblTempo_1.setForeground(Color.BLACK);
		lblTempo_1.setBounds(203, 75, 66, 15);
		novapanel.add(lblTempo_1);
		
		JLabel lblPreo = new JLabel("Preço:");
		lblPreo.setForeground(Color.BLACK);
		lblPreo.setBounds(281, 75, 66, 15);
		novapanel.add(lblPreo);
		
		JLabel lblMaisRpido = new JLabel("Mais rápido:");
		lblMaisRpido.setForeground(Color.BLACK);
		lblMaisRpido.setBounds(12, 211, 98, 15);
		novapanel.add(lblMaisRpido);
		
		JLabel lblMaiorBenefcio = new JLabel("Maior benefício:");
		lblMaiorBenefcio.setForeground(Color.BLACK);
		lblMaiorBenefcio.setBounds(12, 303, 117, 15);
		novapanel.add(lblMaiorBenefcio);
		
		JButton btnVoltar = new JButton("Voltar");
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Removendo painéis:
				basepanel.removeAll();
				basepanel.repaint();
				basepanel.revalidate();
				//Adicionando painéis:
				basepanel.add(entregapanel);
				basepanel.repaint();
				basepanel.revalidate();
			}
		});
		btnVoltar.setBounds(12, 409, 114, 25);
		novapanel.add(btnVoltar);
		
		JButton btnEnviar_1 = new JButton("Enviar");
		btnEnviar_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String placa;
				boolean result, entregaresult;
				placa = frota.procuratipo(envianome1);
				result = frota.UsaVeiculo(placa);
				pegamargem.EnviaEntrega(envianome1,placa, enviatempo1, enviapreco1);
				
				if(result==true) {
					// FOi possível enviar o veículo
					JOptionPane.showMessageDialog(novapanel, "O veículo foi enviado com sucesso!");
				}
				else {
					// Não foi possivel enviar o veiculo
					JOptionPane.showMessageDialog(novapanel, "O veículo não pode ser enviado!");
				}
				
				//Removendo painéis:
				basepanel.removeAll();
				basepanel.repaint();
				basepanel.revalidate();
				//Adicionando painéis:
				basepanel.add(entregapanel);
				basepanel.repaint();
				basepanel.revalidate();
				
			}
		});
		btnEnviar_1.setBounds(269, 147, 114, 25);
		novapanel.add(btnEnviar_1);
		
		JButton btnEnviar_2 = new JButton("Enviar");
		btnEnviar_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boolean result;
				String placa;
				placa = frota.procuratipo(envianome2);
				result = frota.UsaVeiculo(placa);
				pegamargem.EnviaEntrega(envianome2,placa, enviatempo2, enviapreco2);
				
				if(result==true) {
					// FOi possível enviar o veículo
					JOptionPane.showMessageDialog(novapanel, "O veículo foi enviado com sucesso!");
				}
				else {
					// Não foi possivel enviar o veiculo
					JOptionPane.showMessageDialog(novapanel, "O veículo não pode ser enviado!");
				}
				
				//Removendo painéis:
				basepanel.removeAll();
				basepanel.repaint();
				basepanel.revalidate();
				//Adicionando painéis:
				basepanel.add(entregapanel);
				basepanel.repaint();
				basepanel.revalidate();
			}
		});
		btnEnviar_2.setBounds(269, 236, 114, 25);
		novapanel.add(btnEnviar_2);
		
		JButton btnEnviar_3 = new JButton("Enviar");
		btnEnviar_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String placa;
				placa = frota.procuratipo(envianome3);
				boolean result = frota.UsaVeiculo(placa);
				pegamargem.EnviaEntrega(envianome3,placa, enviatempo3, enviapreco3);
				
				if(result==true) {
					// FOi possível enviar o veículo
					JOptionPane.showMessageDialog(novapanel, "O veículo foi enviado com sucesso!");
				}
				else {
					// Não foi possivel enviar o veiculo
					JOptionPane.showMessageDialog(novapanel, "O veículo não pode ser enviado!");
				}
				
				//Removendo painéis:
				basepanel.removeAll();
				basepanel.repaint();
				basepanel.revalidate();
				//Adicionando painéis:
				basepanel.add(entregapanel);
				basepanel.repaint();
				basepanel.revalidate();
			}
		});
		btnEnviar_3.setBounds(269, 330, 114, 25);
		novapanel.add(btnEnviar_3);
		
		JPanel financeiropanel = new JPanel();
		financeiropanel.setBackground(Color.GRAY);
		basepanel.add(financeiropanel, "name_3511933607310");
		financeiropanel.setLayout(null);
		
		JLabel lblMargemDeLucro = new JLabel("Margem de Lucro");
		lblMargemDeLucro.setForeground(Color.BLACK);
		lblMargemDeLucro.setFont(new Font("Ubuntu", Font.BOLD, 20));
		lblMargemDeLucro.setBounds(12, 12, 182, 15);
		financeiropanel.add(lblMargemDeLucro);
		
		JLabel lblNovaMargemDe = new JLabel("Nova margem de lucro:");
		lblNovaMargemDe.setForeground(Color.BLACK);
		lblNovaMargemDe.setBounds(22, 39, 182, 15);
		financeiropanel.add(lblNovaMargemDe);
		
		textFieldML = new JTextField();
		textFieldML.setBounds(189, 37, 124, 19);
		financeiropanel.add(textFieldML);
		textFieldML.setColumns(10);
		
		DecimalFormat pf = new DecimalFormat("0");
		float pegmarg = pegamargem.PegaMargemLucro()*100;
		textFieldML.setText(pf.format(pegmarg));
		
		JLabel label_7 = new JLabel("%");
		label_7.setForeground(Color.BLACK);
		label_7.setBounds(331, 39, 34, 15);
		financeiropanel.add(label_7);
		
		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String saux = textFieldML.getText();
				float faux = Float.valueOf(saux.trim()).floatValue();
				boolean resultado = pegamargem.GravaMargemLucro(faux);
				if(resultado == true) {
					//Aparecer mensagem de que deu certo
					JOptionPane.showMessageDialog(financeiropanel, "Margem de lucro guardada com sucesso!");
				}
				else {
					//Aparecer mensagem de que deu errado
					JOptionPane.showMessageDialog(financeiropanel, "Não foi possível guardar essa margem de lucro!");
				}
			}
		});
		btnSalvar.setForeground(Color.BLACK);
		btnSalvar.setBounds(251, 68, 114, 25);
		financeiropanel.add(btnSalvar);
		
		JLabel lblRelatrios = new JLabel("Relatórios:");
		lblRelatrios.setForeground(Color.BLACK);
		lblRelatrios.setFont(new Font("Ubuntu", Font.BOLD, 20));
		lblRelatrios.setBounds(12, 105, 116, 19);
		financeiropanel.add(lblRelatrios);
		
		JLabel lblRela1 = new JLabel("1 - ");
		lblRela1.setBounds(12, 151, 383, 19);
		financeiropanel.add(lblRela1);
		
		JLabel lblRela2 = new JLabel("2 - ");
		lblRela2.setBounds(12, 182, 383, 19);
		financeiropanel.add(lblRela2);
		
		JLabel lblRela3 = new JLabel("3 - ");
		lblRela3.setBounds(12, 213, 383, 19);
		financeiropanel.add(lblRela3);
		
		JLabel lblRela4 = new JLabel("4 - ");
		lblRela4.setBounds(12, 244, 394, 20);
		financeiropanel.add(lblRela4);
		
		JLabel lblRela5 = new JLabel("5 - ");
		lblRela5.setBounds(12, 276, 394, 19);
		financeiropanel.add(lblRela5);
		
		JLabel lblRela6 = new JLabel("6 - ");
		lblRela6.setBounds(12, 307, 394, 19);
		financeiropanel.add(lblRela6);
		
		JLabel lblRela7 = new JLabel("7 - ");
		lblRela7.setBounds(12, 338, 383, 19);
		financeiropanel.add(lblRela7);
		
		JLabel lblRela8 = new JLabel("8 - ");
		lblRela8.setBounds(12, 369, 394, 19);
		financeiropanel.add(lblRela8);
		
		JLabel lblRela9 = new JLabel("9 - ");
		lblRela9.setBounds(12, 396, 394, 19);
		financeiropanel.add(lblRela9);
		
		JLabel lblRela10 = new JLabel("10 - ");
		lblRela10.setBounds(12, 427, 394, 19);
		financeiropanel.add(lblRela10);
		
		JButton btnGerarRelatrios = new JButton("Gerar relatórios");
		btnGerarRelatrios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ArrayList<String> entregas;
				int tamanho;
				// Pegar tudo que tem no arquivo de entregas
				entregas = pegamargem.PegaRelatorio();
				tamanho = entregas.size();
				int posicao=(tamanho-1);
				if(tamanho<1) {
					// Não existem relatórios ainda.
					JOptionPane.showMessageDialog(financeiropanel, "Não há nenhum relatório de entrega!");
				}
				else if(tamanho>0) {
					// Existem relatorios. Pegá-los enquanto existirem e colocar nas labels
					lblRela1.setText("1 - "+ entregas.get(posicao));
					posicao--;
					if(posicao>0) {
						lblRela2.setText("2 - "+entregas.get(posicao));
					}
					posicao--;
					if(posicao>0) {
						lblRela3.setText("3 - "+entregas.get(posicao));
					}
					posicao--;
					if(posicao>0) {
						lblRela4.setText("4 - "+entregas.get(posicao));
					}
					posicao--;
					if(posicao>0) {
						lblRela5.setText("5 - "+entregas.get(posicao));
					}
					posicao--;
					if(posicao>0) {
						lblRela6.setText("6 - "+entregas.get(posicao));
					}
					posicao--;
					if(posicao>0) {
						lblRela7.setText("7 - "+entregas.get(posicao));
					}
					posicao--;
					if(posicao>0) {
						lblRela8.setText("8 - "+entregas.get(posicao));
					}
					posicao--;
					if(posicao>0) {
						lblRela9.setText("9 - "+entregas.get(posicao));
					}
					posicao--;
					if(posicao>0) {
						lblRela10.setText("10 - "+entregas.get(posicao));
					}
				
				}
				
			}
		});
		btnGerarRelatrios.setForeground(Color.BLACK);
		btnGerarRelatrios.setBounds(218, 105, 147, 25);
		financeiropanel.add(btnGerarRelatrios);
		
		JLabel lblNomeTempoPreo = new JLabel("Nome          Tempo          Preço          Placa");
		lblNomeTempoPreo.setBounds(54, 136, 295, 19);
		financeiropanel.add(lblNomeTempoPreo);
		
		JButton btnVeculos = new JButton("Veículos");
		btnVeculos.setForeground(Color.BLACK);
		btnVeculos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Removendo painéis:
				basepanel.removeAll();
				basepanel.repaint();
				basepanel.revalidate();
				//Adicionando painéis:
				basepanel.add(veicpanel);
				basepanel.repaint();
				basepanel.revalidate();
			}
		});
		btnVeculos.setFont(new Font("Ubuntu", Font.BOLD, 16));
		
		JButton btnEntregas = new JButton("Entregas");
		btnEntregas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Removendo painéis:
				basepanel.removeAll();
				basepanel.repaint();
				basepanel.revalidate();
				//Adicionando painéis:
				basepanel.add(entregapanel);
				basepanel.repaint();
				basepanel.revalidate();
			}
		});
		btnEntregas.setForeground(Color.BLACK);
		btnEntregas.setFont(new Font("Ubuntu", Font.BOLD, 16));
		
		JButton btnFinanceiro = new JButton("Financeiro");
		btnFinanceiro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Removendo painéis:
				basepanel.removeAll();
				basepanel.repaint();
				basepanel.revalidate();
				//Adicionando painéis:
				basepanel.add(financeiropanel);
				basepanel.repaint();
				basepanel.revalidate();
			}
		});
		btnFinanceiro.setForeground(Color.BLACK);
		btnFinanceiro.setFont(new Font("Ubuntu", Font.BOLD, 16));
		GroupLayout gl_controlepanel = new GroupLayout(controlepanel);
		gl_controlepanel.setHorizontalGroup(
			gl_controlepanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_controlepanel.createSequentialGroup()
					.addContainerGap(27, Short.MAX_VALUE)
					.addGroup(gl_controlepanel.createParallelGroup(Alignment.LEADING)
						.addComponent(btnFinanceiro)
						.addGroup(gl_controlepanel.createParallelGroup(Alignment.TRAILING)
							.addComponent(btnEntregas)
							.addComponent(btnVeculos)))
					.addGap(21))
		);
		gl_controlepanel.setVerticalGroup(
			gl_controlepanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_controlepanel.createSequentialGroup()
					.addGap(78)
					.addComponent(btnVeculos)
					.addGap(85)
					.addComponent(btnEntregas)
					.addGap(101)
					.addComponent(btnFinanceiro)
					.addContainerGap(95, Short.MAX_VALUE))
		);
		controlepanel.setLayout(gl_controlepanel);
		frame.getContentPane().setLayout(groupLayout);
	}
}
