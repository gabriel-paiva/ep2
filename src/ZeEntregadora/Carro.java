package ZeEntregadora;

public class Carro extends Veiculos {
	public Carro(String placa, String combustivel) {
		setPlaca(placa);
		setCombustivel(combustivel);
		setCargamax(360.0f);
		setVelmed(100.0f);
		
		if(combustivel == "Gasolina") {
			setRendimento(14.0f);
			setReducao(0.025f);
			setPrecocombustivel(4.449f);
		}
		else {
			setRendimento(12.0f);
			setReducao(0.0231f);
			setPrecocombustivel(3.499f);
		}
	}
}
