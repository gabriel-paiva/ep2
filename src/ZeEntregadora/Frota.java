package ZeEntregadora;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class Frota {
	// Criar diretório e arquivos de dados
	public void CriaArquivos() {
		// Verificar se a pasta de arquivos ja existe, senão, criá-la.
				java.io.File diretorio = new java.io.File("data");
				if(diretorio.exists()==false) {
					diretorio.mkdir();
				}
				// Verificar se o arquivo de veículos disponiveis já existe.
				java.io.File disponivel = new java.io.File("data/disponiveis.dat");
				if(disponivel.exists()==false) {
					try {
						disponivel.createNewFile();
					} 
					catch(IOException ex) {
					}
				}
				
				// Verificar se o arquivo de veículos usados já existe.
				java.io.File usado = new java.io.File("data/usados.dat");
				if(usado.exists()==false) {
					try {
						usado.createNewFile();
					} 
					catch(IOException ex) {
					}
				}
	}//Fim CriaArquivos
	
	// Criar veículo na lista de Disponiveis
	public void CriaVeiculo(String tipo, String placa, String combustivel) {
		
		ArrayList<Veiculos> listaDisponiveis = new ArrayList<>();
		Veiculos novo;
		//Carregar o arraylist do arquivo, caso não esteja vazio
		java.io.File arquivo = new java.io.File("data/disponiveis.dat");
		try {
			if(arquivo.canRead()==true) {
				if(arquivo.length()>0) {
				ObjectInputStream leitor = new ObjectInputStream(new FileInputStream(arquivo));
				listaDisponiveis = (ArrayList<Veiculos>)leitor.readObject();
				leitor.close();
			}
			}
		}
		catch(Exception e){
            System.out.println(e.toString());
		}
		
		//Cria novo objeto veiculo
		if(tipo == "Moto") {
			novo = new Moto(placa, combustivel);
		}
		else if (tipo == "Carro") {
			novo = new Carro(placa, combustivel);
		}
		else if(tipo == "Van") {
			novo = new Van(placa);
		}
		else {
			novo = new Carreta(placa);
		}
		
		listaDisponiveis.add(novo);
		
		// Salvar o arraylist de novo no arquivo;
		
		try {
			if(arquivo.canWrite()==true) {
				ObjectOutputStream gravador = new ObjectOutputStream(new FileOutputStream(arquivo));
				gravador.writeObject(listaDisponiveis);
				gravador.close();
			}
		}
		catch(Exception e){
             System.out.println(e.toString());
		}
	}

	// Procura tipo de veículo
	public String procuratipo(String tipo) {
		String placa = null;
		float comparador;
		if (tipo == "Carreta") {
			comparador = 60.0f;
		}
		else if(tipo=="Van") {
			comparador = 80.0f;
		}
		else if(tipo=="Carro") {
			comparador = 100.0f;
		}
		else {
			comparador = 110.0f;
		}
		// Abrir o arquivo
		ArrayList<Veiculos> listaDisponiveis = new ArrayList<>();
		
		//Carregar o arraylist do arquivo, caso não esteja vazio
		java.io.File arquivo = new java.io.File("data/disponiveis.dat");
		try {
			if(arquivo.canRead()==true) {
				ObjectInputStream leitor = new ObjectInputStream(new FileInputStream(arquivo));
				listaDisponiveis = (ArrayList<Veiculos>)leitor.readObject();
				leitor.close();
			}
		}
		catch(Exception e){
            System.out.println(e.toString());
		}
		
		// Procurar a placa e retornar a posição
		for (int i = 0; i < listaDisponiveis.size();i++) {
			if (comparador==listaDisponiveis.get(i).getVelmed()) {
				placa = listaDisponiveis.get(i).getPlaca();
				break;
			}
		}
		
		return placa;
	}
	
	// Procurar veículo na lista de Disponíveis (retorna sua posição no array)
	public int ProcuraVeiculoDisp(String placa) {
		
		ArrayList<Veiculos> listaDisponiveis = new ArrayList<>();
		int pos = -1;
		
		//Carregar o arraylist do arquivo, caso não esteja vazio
		java.io.File arquivo = new java.io.File("data/disponiveis.dat");
		try {
			if(arquivo.canRead()==true) {
				ObjectInputStream leitor = new ObjectInputStream(new FileInputStream(arquivo));
				listaDisponiveis = (ArrayList<Veiculos>)leitor.readObject();
				leitor.close();
			}
		}
		catch(Exception e){
            System.out.println(e.toString());
		}
		
		// Procurar a placa e retornar a posição
		for (int i = 0; i < listaDisponiveis.size();i++) {
			if (placa.equals(listaDisponiveis.get(i).getPlaca())) {
				pos = i;
			}
		}
		
		return pos;
	}
	
	// Procurar veículo na lista de Usados
	public int ProcuraVeiculoUsado(String placa) {
		ArrayList<Veiculos> listaUsados = new ArrayList<>();
		int pos = -1;
		
		//Carregar o arraylist do arquivo, caso não esteja vazio
		java.io.File arquivo = new java.io.File("data/usados.dat");
		try {
			if(arquivo.canRead()==true) {
				if(arquivo.length()>0) {
				ObjectInputStream leitor = new ObjectInputStream(new FileInputStream(arquivo));
				listaUsados = (ArrayList<Veiculos>)leitor.readObject();
				leitor.close();
				}
			}
		}
		catch(Exception e){
            System.out.println(e.toString());
		}
		
		// Procurar a placa e retornar a posição
		for (int i = 0; i < listaUsados.size();i++) {
			if (placa.equals(listaUsados.get(i).getPlaca())) {
				pos = i;
				break;
			}
		}
		
		return pos;
	}
	
	// Apagar veiculo da lista de disponiveis
	public boolean ApagaVeiculo(String placa) {
		int pos;
		boolean result = true;
		pos = ProcuraVeiculoDisp(placa);
		
		if (pos<0) {
			result = false;
		}
		else {
			// Abre o arquivo
			ArrayList<Veiculos> listaDisponiveis = new ArrayList<>();
			
			//Carregar o arraylist do arquivo, caso não esteja vazio
			java.io.File arquivo = new java.io.File("data/disponiveis.dat");
			try {
				if(arquivo.canRead()==true) {
					ObjectInputStream leitor = new ObjectInputStream(new FileInputStream(arquivo));
					listaDisponiveis = (ArrayList<Veiculos>)leitor.readObject();
					leitor.close();
				}
			}
			catch(Exception e){
	            System.out.println(e.toString());
			}
			// Apaga o veículo na posicao
			listaDisponiveis.remove(pos);
			// Salva e fecha arquivo.
			try {
				if(arquivo.canWrite()==true) {
					ObjectOutputStream gravador = new ObjectOutputStream(new FileOutputStream(arquivo));
					gravador.writeObject(listaDisponiveis);
					gravador.close();
				}
			}
			catch(Exception e){
	             System.out.println(e.toString());
			}
		}
		return result;
	}
	
	// Usar veículo (ListaDisponiveis -> ListaUsados)
	public boolean UsaVeiculo (String placa) {
		int pos;
		boolean result = true;
		pos = ProcuraVeiculoDisp(placa);
		if(pos<0) {
			result = false;
		}
		else {
			// Abre o arquivo de Disponíveis
				ArrayList<Veiculos> listaDisponiveis = new ArrayList<>();
				Veiculos copia;
						
			//Carregar o arraylist do arquivo, caso não esteja vazio
				java.io.File arquivo = new java.io.File("data/disponiveis.dat");
				try {
					if(arquivo.canRead()==true) {
						if(arquivo.length()>0) {
						ObjectInputStream leitor = new ObjectInputStream(new FileInputStream(arquivo));
						listaDisponiveis = (ArrayList<Veiculos>)leitor.readObject();
						leitor.close();
						}
					}
				}
				catch(Exception e){
				    System.out.println(e.toString());
				}
				// Apaga o veículo na posicao
				copia = listaDisponiveis.get(pos);
				listaDisponiveis.remove(pos);
				// Salva e fecha arquivo.
				try {
					if(arquivo.canWrite()==true) {
						ObjectOutputStream gravador = new ObjectOutputStream(new FileOutputStream(arquivo));
						gravador.writeObject(listaDisponiveis);
						gravador.close();
					}
				}
				catch(Exception e){
				    System.out.println(e.toString());
				}
			// Abrir o arquivo de usados.
			ArrayList<Veiculos> listaUsados = new ArrayList<>();
			//Carregar o arraylist do arquivo, caso não esteja vazio
			java.io.File arquivo_used = new java.io.File("data/usados.dat");
			try {
				if(arquivo_used.canRead()==true) {
					if(arquivo_used.length()>0) {
					ObjectInputStream leitor_used = new ObjectInputStream(new FileInputStream(arquivo_used));
					listaUsados = (ArrayList<Veiculos>)leitor_used.readObject();
					leitor_used.close();
					}
				}
			}
			catch(Exception e){
			    System.out.println(e.toString());
			}
			// Guardar o objeto dentro de usados
			listaUsados.add(copia);
			// Salvar e fechar usados
			try {
				if(arquivo_used.canWrite()==true) {
					ObjectOutputStream gravador_used = new ObjectOutputStream(new FileOutputStream(arquivo_used));
					gravador_used.writeObject(listaUsados);
					gravador_used.close();
				}
			}
			catch(Exception e){
			    System.out.println(e.toString());
			}
		}
		return result;
	}
	
	// Retornar veículo (ListaUsados -> ListaDisponiveis)
	public boolean RetornaVeiculo(String placa) {
		int pos;
		boolean result = true;
		pos = ProcuraVeiculoUsado(placa);
		if(pos<0) {
			result = false;
		}
		else {
			// Abre o arquivo de Usados
				ArrayList<Veiculos> listaUsados = new ArrayList<>();
				Veiculos copia;
						
			//Carregar o arraylist do arquivo, caso não esteja vazio
				java.io.File arquivo = new java.io.File("data/usados.dat");
				try {
					if(arquivo.canRead()==true) {
						if(arquivo.length()>0) {
						ObjectInputStream leitor = new ObjectInputStream(new FileInputStream(arquivo));
						listaUsados = (ArrayList<Veiculos>)leitor.readObject();
						leitor.close();
						}
					}
				}
				catch(Exception e){
				    System.out.println(e.toString());
				}
				// Apaga o veículo na posicao
				copia = listaUsados.get(pos);
				listaUsados.remove(pos);
				// Salva e fecha arquivo.
				try {
					if(arquivo.canWrite()==true) {
						ObjectOutputStream gravador = new ObjectOutputStream(new FileOutputStream(arquivo));
						gravador.writeObject(listaUsados);
						gravador.close();
					}
				}
				catch(Exception e){
				    System.out.println(e.toString());
				}
			// Abrir o arquivo de Disponiveis.
			ArrayList<Veiculos> listaDisponiveis = new ArrayList<>();
			//Carregar o arraylist do arquivo, caso não esteja vazio
			java.io.File arquivo_used = new java.io.File("data/disponiveis.dat");
			try {
				if(arquivo_used.canRead()==true) {
					if(arquivo_used.length()>0) {
					ObjectInputStream leitor_used = new ObjectInputStream(new FileInputStream(arquivo_used));
					listaDisponiveis = (ArrayList<Veiculos>)leitor_used.readObject();
					leitor_used.close();
					}
				}
			}
			catch(Exception e){
			    System.out.println(e.toString());
			}
			// Guardar o objeto dentro de usados
			listaDisponiveis.add(copia);
			// Salvar e fechar usados
			try {
				if(arquivo_used.canWrite()==true) {
					ObjectOutputStream gravador_used = new ObjectOutputStream(new FileOutputStream(arquivo_used));
					gravador_used.writeObject(listaDisponiveis);
					gravador_used.close();
				}
			}
			catch(Exception e){
			    System.out.println(e.toString());
			}
		}
		return result;
	}
}// Fim da classe frota
