package ZeEntregadora;

public class Van extends Veiculos {
	public Van(String placa) {
		setPlaca(placa);
		setCombustivel("Diesel");
		setRendimento(10.0f);
		setCargamax(3500.0f);
		setVelmed(80.0f);
		setReducao(0.001f);
		setPrecocombustivel(3.869f);
	}
}
