package ZeEntregadora;

import java.io.Serializable;

public abstract class Veiculos implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String combustivel;
	private String placa;
	private float rendimento;
	private float cargamax;
	private float velmed;
	private float reducao;
	private float precocombustivel;
	
	public float getPrecocombustivel() {
		return precocombustivel;
	}
	public void setPrecocombustivel(float precocombustivel) {
		this.precocombustivel = precocombustivel;
	}
	public String getCombustivel() {
		return combustivel;
	}
	public void setCombustivel(String combustivel) {
		this.combustivel = combustivel;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public float getRendimento() {
		return rendimento;
	}
	public void setRendimento(float rendimento) {
		this.rendimento = rendimento;
	}
	public float getCargamax() {
		return cargamax;
	}
	public void setCargamax(float cargamax) {
		this.cargamax = cargamax;
	}
	public float getVelmed() {
		return velmed;
	}
	public void setVelmed(float velmed) {
		this.velmed = velmed;
	}
	public float getReducao() {
		return reducao;
	}
	public void setReducao(float reducao) {
		this.reducao = reducao;
	}
	
	
	
	
}
