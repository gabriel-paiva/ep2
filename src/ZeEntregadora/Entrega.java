package ZeEntregadora;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class Entrega {
	private float margemdelucro;
	private float carga;
	private float distancia;
	private float tempo;
	private String menor_custo;
	private float menor_custo_tempo;
	private float menor_custo_preco;
	private String mais_rapido;
	private float mais_rapido_tempo;
	private float mais_rapido_preco;
	private String mais_benefico;
	private float mais_benefico_tempo;
	private float mais_benefico_preco;
	
	
	
	public String getMenor_custo() {
		return menor_custo;
	}

	public void setMenor_custo(String menor_custo) {
		this.menor_custo = menor_custo;
	}

	public float getMenor_custo_tempo() {
		return menor_custo_tempo;
	}

	public void setMenor_custo_tempo(float menor_custo_tempo) {
		this.menor_custo_tempo = menor_custo_tempo;
	}

	public float getMenor_custo_preco() {
		return menor_custo_preco;
	}

	public void setMenor_custo_preco(float menor_custo_preco) {
		this.menor_custo_preco = menor_custo_preco;
	}

	public String getMais_rapido() {
		return mais_rapido;
	}

	public void setMais_rapido(String mais_rapido) {
		this.mais_rapido = mais_rapido;
	}

	public float getMais_rapido_tempo() {
		return mais_rapido_tempo;
	}

	public void setMais_rapido_tempo(float mais_rapido_tempo) {
		this.mais_rapido_tempo = mais_rapido_tempo;
	}

	public float getMais_rapido_preco() {
		return mais_rapido_preco;
	}

	public void setMais_rapido_preco(float mais_rapido_preco) {
		this.mais_rapido_preco = mais_rapido_preco;
	}

	public String getMais_benefico() {
		return mais_benefico;
	}

	public void setMais_benefico(String mais_benefico) {
		this.mais_benefico = mais_benefico;
	}

	public float getMais_benefico_tempo() {
		return mais_benefico_tempo;
	}

	public void setMais_benefico_tempo(float mais_benefico_tempo) {
		this.mais_benefico_tempo = mais_benefico_tempo;
	}

	public float getMais_benefico_preco() {
		return mais_benefico_preco;
	}

	public void setMais_benefico_preco(float mais_benefico_preco) {
		this.mais_benefico_preco = mais_benefico_preco;
	}

	public Entrega(float carga, float distancia, float tempo) {
		this.carga = carga;
		this.distancia = distancia;
		this.tempo = tempo;
	}
	
	public float getMargemdelucro() {
		return margemdelucro;
	}
	public void setMargemdelucro(float margemdelucro) {
		this.margemdelucro = margemdelucro;
	}
	public float getCarga() {
		return carga;
	}
	public void setCarga(float carga) {
		this.carga = carga;
	}
	public float getDistancia() {
		return distancia;
	}
	public void setDistancia(float distancia) {
		this.distancia = distancia;
	}
	public float getTempo() {
		return tempo;
	}
	public void setTempo(float tempo) {
		this.tempo = tempo;
	}
	
	public void CriaArquivosDados() {
		// Verificar se a pasta de arquivos ja existe, senão, criá-la.
				java.io.File diretorio = new java.io.File("data");
				if(diretorio.exists()==false) {
					diretorio.mkdir();
				}
				// Verificar se o arquivo de margem de lucro já existe.
				java.io.File margem = new java.io.File("data/margem.dat");
				if(margem.exists()==false) {
					try {
						margem.createNewFile();
						GravaMargemLucro(0.0f);
					} 
					catch(IOException ex) {
					}
				}
				
				// Verificar se o arquivo de entregas já existe.
				java.io.File relatorio = new java.io.File("data/entregas.dat");
				if(relatorio.exists()==false) {
					try {
						relatorio.createNewFile();
					} 
					catch(IOException ex) {
					}
				}
				
	}//Fim CriaArquivos
	
	
	// Calcula o custo de operação e retorna um array[6] com todos os custos para veículos e seus combustíveis.
	public float[] CalculaCusto () {
		float carga, distancia, novoRendimento;
		float[]  custo = new float[6];
		
		carga = getCarga();
		distancia = getDistancia();
		
		// Carreta:
		novoRendimento = (8.0f - (carga*0.0002f));
		custo[0] = ((distancia/novoRendimento)*3.869f);
		
		// Van:
		novoRendimento = (10.0f - (carga*0.001f));
		custo[1] = ((distancia/novoRendimento)*3.869f);
		
		// Carro à gasolina:
		novoRendimento = (14.0f - (carga*0.025f));
		custo[2] = ((distancia/novoRendimento)*4.449f);
		
		// Carro à álcool:
		novoRendimento = (12.0f - (carga*0.0231f));
		custo[3] = ((distancia/novoRendimento)*3.499f);
		
		// Moto à gasolina:
		novoRendimento = (50.0f - (carga*0.3f));
		custo[4] = ((distancia/novoRendimento)*4.449f);
		
		// Moto à álcool:
		novoRendimento = (43.0f - (carga*0.4f));
		custo[5] = ((distancia/novoRendimento)*3.499f);
		
		return custo;
	}
	
	// Calcula Tempo. Retorna um array[4] com todos os tempos.
	public float[] CalculaTempo() {
		float[]  tempos = new float[4];
		float distancia;
		
		distancia = getDistancia();
		
		// Carreta:
		tempos[0] = (distancia/60.0f);
		
		// Van:
		tempos[1] = (distancia/80.0f);
		
		// Carro:
		tempos[2] = (distancia/100.0f);
		
		// Moto:
		tempos[3] = (distancia/110.0f);
		
		return tempos;
	}
	
	// Calcula custo total. Retorna um array[6] com todos os totais.
	public float[] CalculaTotal(float[] custos) {
		float[]  total = new float[6];
		float margem;
		// Pegar a margem de lucro do arquivo.
		setMargemdelucro(PegaMargemLucro());
		margem = getMargemdelucro();

		for (int i = 0; i<6; i++) {
			total[i] = (custos[i]*(1+margem));
		}
		
		return total;
	}
	
	// Calcula custo beneficio. Retorna um array[6] com todos os beneficios.
	public float[] CalculaBeneficio (float[] custos, float[] total) {
		float[]  beneficios = new float[6];
		for (int i= 0; i<6;i++) {
			beneficios[i] = total[i] - custos[i];
		}
		return beneficios;
	}
	
	// Verifica os tipos de veículos disponíveis. Retorna um array[6] com os tipos.
	public int[] VerificaTipos() {
		
		// 0 = Carreta, 1 = Van, 2 = Carro Gasolina, 3 = Carro Alcool
		// 4 = Moto Gasolina, 5 = Moto diesel
		
		int cont_carreta = 0, cont_van = 0, cont_cargas =0, cont_caral = 0, cont_motgas = 0, cont_motal = 0;
		ArrayList<Veiculos> listaDisponiveis = new ArrayList<>();
		int[]  verificador = new int[6];
		
		for (int j=0;j<6;j++) {
			verificador[j] = 1;
		}
		
		//Carregar o arraylist do arquivo, caso não esteja vazio
		java.io.File arquivo = new java.io.File("data/disponiveis.dat");
		try {
			if(arquivo.canRead()==true) {
				ObjectInputStream leitor = new ObjectInputStream(new FileInputStream(arquivo));
				listaDisponiveis = (ArrayList<Veiculos>)leitor.readObject();
				leitor.close();
			}
		}
		catch(Exception e){
            System.out.println(e.toString());
		}
		
		// Verificar a existencia:
		for (int i = 0; i<listaDisponiveis.size();i++) {
			if(listaDisponiveis.get(i).getRendimento() == 8.0f ) {
				// Se for uma carreta:
				cont_carreta++;
			}
			else if(listaDisponiveis.get(i).getRendimento() == 10.0f) {
				// Se for uma van:
				cont_van++;
			}
			else if(listaDisponiveis.get(i).getRendimento() == 14.0f) {
				// Se for um carro à gasolina:
				cont_cargas++;
			}
			else if (listaDisponiveis.get(i).getRendimento() == 12.0f) {
				// Se for um carro à alcool:
				cont_caral++;
			}
			else if (listaDisponiveis.get(i).getRendimento() == 50.0f) {
				// Se for uma moto a gasolina:
				cont_motgas++;
			}
			else {
				// Se for uma moto a alcool:
				cont_motal++;
			}
		}
		
		if(cont_carreta==0) {
			verificador[0] = -1;
		}
		if(cont_van==0) {
			verificador[1] = -1;
		}
		if(cont_cargas==0) {
			verificador[2]=-1;
		}
		if(cont_caral==0) {
			verificador[3]=-1;
		}
		if(cont_motgas==0) {
			verificador[4] = -1;
		}
		if(cont_motal==0) {
			verificador[5] = -1;
		}
		
		return verificador;
	}

	// Verificar entrega e setar os dados de saída.
	public boolean Verifica_entrega() {
		boolean result = true;
		// Pegar dados calculados.
		float[]  lista_custo = new float[6];
		lista_custo = CalculaCusto();
		float[]  lista_total = new float[6];
		lista_total = CalculaTotal(lista_custo);
		float[]  lista_beneficio = new float[6];
		lista_beneficio = CalculaBeneficio(lista_custo, lista_total);
		float[]  lista_tempos = new float[4];
		lista_tempos = CalculaTempo();
		int[]  lista_disponiveis = new int[6];
		lista_disponiveis = VerificaTipos();
		
		// Verificar carga
		float carga = getCarga();
		if(carga>30000.0f) {
			for (int k = 0; k<6; k++) {
				lista_disponiveis[k] = -1; 
			}
			result = false;
		}
		else if(carga>3500.0f) {
			lista_disponiveis[1] = -1;
			lista_disponiveis[2] = -1;
			lista_disponiveis[3] = -1;
			lista_disponiveis[4] = -1;
			lista_disponiveis[5] = -1;
		}
		
		else if(carga>360.0f) {
			lista_disponiveis[2] = -1;
			lista_disponiveis[3] = -1;
			lista_disponiveis[4] = -1;
			lista_disponiveis[5] = -1;
		}
		else if(carga>50.0f) {
			lista_disponiveis[4] = -1;
			lista_disponiveis[5] = -1;
		}
		
		// Verificar tempo:
		float tempo = getTempo();
		if(tempo<lista_tempos[0]) {
			lista_disponiveis[0] = -1;
		}
		if(tempo<lista_tempos[1]) {
			lista_disponiveis[1] = -1;
		}
		if(tempo<lista_tempos[2]) {
			lista_disponiveis[2] = -1;
			lista_disponiveis[3] = -1;
		}
		if(tempo<lista_tempos[3]) {
			lista_disponiveis[4] = -1;
			lista_disponiveis[5] = -1;
		}
		
		// Calcula menor custos
		int conta_um = 0, pos_menor=0;
		float aux_menor=0f;
		for (int c=0;c<6;c++) {
			if(lista_disponiveis[c]==1) {
				conta_um++;
				if(conta_um==1) {
					aux_menor = lista_custo[c];
					pos_menor = c;
				}
				else {
					if(lista_custo[c]<aux_menor) {
						aux_menor = lista_custo[c];
						pos_menor = c;
					}
				}
			}
		}
		// Decidir o tipo de veículo do menor custo:
		if(pos_menor==0) {
			//É uma carreta
			setMenor_custo("Carreta");
			setMenor_custo_tempo(lista_tempos[0]);
			setMenor_custo_preco(lista_total[0]);
		}
		else if (pos_menor==1) {
			//É uma van
			setMenor_custo("Van");
			setMenor_custo_tempo(lista_tempos[1]);
			setMenor_custo_preco(lista_total[1]);
		}
		else if (pos_menor==2) {
			// É um carro à gasolina:
			setMenor_custo("Carro");
			setMenor_custo_tempo(lista_tempos[2]);
			setMenor_custo_preco(lista_total[2]);
		}
		else if (pos_menor==3) {
			// É um carro à álcool:
			setMenor_custo("Carro");
			setMenor_custo_tempo(lista_tempos[2]);
			setMenor_custo_preco(lista_total[3]);
		}
		else if (pos_menor==4) {
			// É uma moto à gasolina:
			setMenor_custo("Moto");
			setMenor_custo_tempo(lista_tempos[3]);
			setMenor_custo_preco(lista_total[4]);
		}
		else {
			// É uma moto à alcool:
			setMenor_custo("Moto");
			setMenor_custo_tempo(lista_tempos[3]);
			setMenor_custo_preco(lista_total[5]);
		}
		
		// Calcular mais rápido:
			
		if(lista_disponiveis[5]==1) {
			// É a moto à álcool:
			setMais_rapido("Moto");
			setMais_rapido_tempo(lista_tempos[3]);
			setMais_rapido_preco(lista_total[5]);
		}
		else if (lista_disponiveis[4]==1) {
			// É a moto à gasolina:
			setMais_rapido("Moto");
			setMais_rapido_tempo(lista_tempos[3]);
			setMais_rapido_preco(lista_total[4]);
		}
		else if (lista_disponiveis[3]==1) {
			// É o carro à alcool
			setMais_rapido("Carro");
			setMais_rapido_tempo(lista_tempos[2]);
			setMais_rapido_preco(lista_total[3]);
		}
		else if (lista_disponiveis[2]==1) {
			// É o carro à gasolina
			setMais_rapido("Carro");
			setMais_rapido_tempo(lista_tempos[2]);
			setMais_rapido_preco(lista_total[2]);
		}
		else if (lista_disponiveis[1]==1) {
			// É a Van
			setMais_rapido("Van");
			setMais_rapido_tempo(lista_tempos[1]);
			setMais_rapido_preco(lista_total[1]);
		}
		else if (lista_disponiveis[0]==1) {
			// É a carreta
			setMais_rapido("Carreta");
			setMais_rapido_tempo(lista_tempos[0]);
			setMais_rapido_preco(lista_total[0]);
		}
		else {
			result = false;
		}
		
		// Calcular o maior custo beneficio
		float aux_ben = 0;
		int conta1=0, pos_ben=0;
		for (int b = 0; b<6; b++) {
			if(lista_disponiveis[b]==1) {
				conta1++;
				if(conta1==1) {
					aux_ben=lista_beneficio[b];
					pos_ben=b;
				}
				else {
					if(lista_beneficio[b]<aux_ben) {
						aux_ben = lista_beneficio[b];
						pos_ben=b;
					}
					
				}
			}
		}
		// Decidir o tipo de veículo do maior beneficio:
				if(pos_ben==0) {
					//É uma carreta
					setMais_benefico("Carreta");
					setMais_benefico_tempo(lista_tempos[0]);
					setMais_benefico_preco(lista_total[0]);
				}
				else if (pos_ben==1) {
					//É uma van
					setMais_benefico("Van");
					setMais_benefico_tempo(lista_tempos[1]);
					setMais_benefico_preco(lista_total[1]);
				}
				else if (pos_ben==2) {
					// É um carro à gasolina:
					setMais_benefico("Carro");
					setMais_benefico_tempo(lista_tempos[2]);
					setMais_benefico_preco(lista_total[2]);
				}
				else if (pos_ben==3) {
					// É um carro à álcool:
					setMais_benefico("Carro");
					setMais_benefico_tempo(lista_tempos[2]);
					setMais_benefico_preco(lista_total[3]);
				}
				else if (pos_ben==4) {
					// É uma moto à gasolina:
					setMais_benefico("Moto");
					setMais_benefico_tempo(lista_tempos[3]);
					setMais_benefico_preco(lista_total[4]);
				}
				else {
					// É uma moto à alcool:
					setMais_benefico("Moto");
					setMais_benefico_tempo(lista_tempos[3]);
					setMais_benefico_preco(lista_total[5]);
				}
		return result;
	}
	
	// Criar arquivo de margem de lucro.
	public boolean GravaMargemLucro(float margemlucro){
		boolean status = false;
		margemlucro = margemlucro/100;
		
		// Criar um novo arquivo na pasta data.
		java.io.File margem = new java.io.File("data/margem.dat");
		if(margem.exists()==false) {
			try {
				margem.createNewFile();
			} 
			catch(IOException ex) {
			}
		}
		// Guardar a margem de lucro no arquivo.
		try {
			if(margem.canWrite()==true) {
				ObjectOutputStream gravador = new ObjectOutputStream(new FileOutputStream(margem));
				gravador.writeObject(margemlucro);
				gravador.close();
			}
		}
		catch(Exception e){
             System.out.println(e.toString());
		}
		
		status = true;
		
		return status;
	}
	
	// Pegar a margem de lucro do arquivo
	public float PegaMargemLucro() {
		float margemlucro=0f;
		
		java.io.File margem = new java.io.File("data/margem.dat");
		//Carregar o arraylist do arquivo, caso não esteja vazio
		try {
			if(margem.canRead()==true) {
				ObjectInputStream leitor = new ObjectInputStream(new FileInputStream(margem));
					margemlucro = (float)leitor.readObject();
					leitor.close();
			}
		}
		catch(Exception e){
		     System.out.println(e.toString());
		}
		return margemlucro;
	}

	// Envia entrega, grava entrega enviada em um arquivo de relatorio.
	public void EnviaEntrega(String nome, String placa, float tempo, float preco) {
		String stempo = Float.toString(tempo);
		String spreco = Float.toString(preco);
		String saidaString = nome + "     " + stempo + "     " + spreco + "     " + placa;
		ArrayList<String> listaEntregas = new ArrayList<>();
		
		// Verificar se o arquivo existe e carregar o arraylist
		java.io.File arqRela = new java.io.File("data/entregas.dat");
		try {
			if(arqRela.canRead()==true) {
				if(arqRela.length()>0) {
				ObjectInputStream leitor = new ObjectInputStream(new FileInputStream(arqRela));
				listaEntregas = (ArrayList<String>)leitor.readObject();
				leitor.close();
				}
			}
		}
		catch(Exception e){
		    System.out.println(e.toString());
		}
		// Adicionar string no arraylist
		listaEntregas.add(saidaString);
		// Salvar arraylist no arquivo novamente.
		try {
			if(arqRela.canWrite()==true) {
				ObjectOutputStream gravador = new ObjectOutputStream(new FileOutputStream(arqRela));
				gravador.writeObject(listaEntregas);
				gravador.close();
			}
		}
		catch(Exception e){
             System.out.println(e.toString());
		}
	}
	
	// Pegar componentes do relatorio:
	public ArrayList<String> PegaRelatorio() {
		ArrayList<String> ultimos = new ArrayList<>();
		// Abre o arquivo do relatorio
		java.io.File relatorio = new java.io.File("data/entregas.dat");
		//Carregar o arraylist do arquivo, caso não esteja vazio
		try {
			if(relatorio.canRead()==true) {
				if(relatorio.length()!=0) {
					
					ObjectInputStream leitor = new ObjectInputStream(new FileInputStream(relatorio));
						ultimos = (ArrayList<String>)leitor.readObject();
					leitor.close();
				}
			}
		}
		catch(Exception e){
		     System.out.println(e.toString());
		}
			return ultimos;
		
	}
}
