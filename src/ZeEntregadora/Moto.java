package ZeEntregadora;

import java.io.Serializable;

public class Moto extends Veiculos {
	public Moto(String placa, String combustivel) {
		setPlaca(placa);
		setCombustivel(combustivel);
		setCargamax(50.0f);
		setVelmed(110.0f);
		
		if(combustivel == "Gasolina") {
			setRendimento(50.0f);
			setReducao(0.3f);
			setPrecocombustivel(4.449f);
		}
		else {
			setRendimento(43.0f);
			setReducao(0.4f);
			setPrecocombustivel(3.499f);
		}
	}
}
