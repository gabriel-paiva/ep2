package ZeEntregadora;

public class Carreta extends Veiculos {
	public Carreta(String placa) {
		setPlaca(placa);
		setCombustivel("Diesel");
		setRendimento(8.0f);
		setCargamax(30000.0f);
		setVelmed(60.0f);
		setReducao(0.0002f);
		setPrecocombustivel(3.869f);
	}
}
