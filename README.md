Tutorial Transportadora:

#Importante:
Diagrama de Classes: encontra-se na pasta raiz do repositório, com o nome: "UML-Transportadora", em formato de imagem PNG.

Executável: Apesar de enviar junto ao trabalho o arquivo executável .jar, não consegui executá-lo em meu computador. Portanto, recomenda-se abrir esse trabalho na IDE eclipse, usando a classe principal "Mainteste.java".

A versão do Java utilizada foi: "11.0.3" 2019-04-16.

#Tutorial:
	Após executar o programa como indicado previamente, o usuário terá acesso à uma janela com dois painéis. À esquerda, um painel de opções para acessar as abas "Veículos", "Entregas" e "Financeiro" e um painél dinâmico, à direita, onde poderá interagir com as opções do programa. Podendo cadastrar e excluir veículos, realizar e finalizar entregas, mudar a margem de lucro e checar os relatórios das últimas entregas.
	Recomendações:
	- Utilize somente valores inteiros (entre zero e cem) para inserir a margem de lucro. Caso deseje usar um valor decimal, separe-o com "." (ponto) e não "," (vírgula).

